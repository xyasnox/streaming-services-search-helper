import React from 'react';
import styled from 'styled-components';

const ServiceCheckBox = styled.input`
  font-weight: bolder;
  outline-style: none;
`;

const CheckBox = ({
                    onCheck,
                  }) =>
  <ServiceCheckBox onClick={() => onCheck()} type='checkbox' />;

export default CheckBox;