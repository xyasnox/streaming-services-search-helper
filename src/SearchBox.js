import React from 'react';
import styled from 'styled-components';
import Input from './Input';
import Button from './Button';
import CheckBox from './CheckBox';

const Box = styled.div`
  padding: 0;
  display: flex;
  margin-top: 25%;
  position: relative;
  outline-style: none;
`;

const FooterBox = styled.div`
  display: flex;
  width: 40%;
  margin-top: 20px;
  position: relative;
  outline-style: none;
  color: #fff;
  font-weight: bolder;
  border-radius: 4px;
  background-color: #1aaf55;
  outline-style: none;
  justify-content: space-between;
`;

const Main = styled.div`
  justify-content: center;
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const Img = styled.img`
  height: 25px;
  width: 25px;
`;

const SearchBox = ({
                     onSearch,
                     onClick,
                     services,
                     onCheck
                   }) =>
  <div>
    <Main>
      <Box>
        <Input onSearch={onSearch} />
        <Button onClick={onClick} />
      </Box>
      <FooterBox>
        {services.map(({ name, imgSrc }) => <div><CheckBox onCheck={() => onCheck(name)} />{name}<Img src={imgSrc} />
        </div>)}
      </FooterBox>
    </Main>
  </div>;

export default SearchBox;