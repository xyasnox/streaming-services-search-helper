const _call = (url, callback, options = {}) => {
  fetch(url, options)
    .then(response => response.json())
    .then(response => callback(response))
};

export const getSpotify = (search, callback) => {
  _call(`https://api.spotify.com/v1/search?query=${search}&type=artist&market=NL&offset=0&limit=20`, callback, {headers: {'Authorization': 'Bearer BQAvtEMPi1c7FIEyI78e-w9cJm7wPUaECNHSz6jCs2_536gSXGvIcj-_I2wCUNCF3pDbzQAuUprF0Wuwh-C3KsLPhaAURqJjuNVlru8lo_KDsPq0-TYUzl-N8beC3r8uOf1ChVVPfYaJ2Ml58CELxO7C5Z53i5TrwTxZUQ'}});
};

export const getItunes = (search, callback) => {
  _call(`https://itunes.apple.com/search?term=${search}&entity=musicArtist`, callback);
};

/*
export const getDeezer = (search, callback) => {
  _call(`https://api.deezer.com/search?q=artist:"${search}"&entity=musicArtist`, callback);
};
*/
