import React from 'react';
import SearchBox from './SearchBox';
import Card from './Card'
import { getSpotify, getItunes } from './helpers/fetch';
import styled from 'styled-components';

const CardColumn = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 20px;
  position: relative;
  outline-style: none;
  font-weight: bolder;
  border-radius: 4px;
  outline-style: none;
  justify-content:: center;
`;

const Div = styled.div`
  display: flex;
  flex-direction: row;
  position: relative;
  justify-content: center;
`;

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listSpot: [],
      listItunes: [],
      search: '',
      services: [
        {
          name: 'Spotify',
          imgSrc: 'https://img.icons8.com/color/50/000000/spotify--v2.png',
          isChecked: false
        },
        {
          name: 'Deezer',
          imgSrc: 'https://img.icons8.com/color/50/000000/deezer.png',
          isChecked: false

        },
        {
          name: 'iTunes',
          imgSrc: 'https://img.icons8.com/ios/50/000000/itunes.png',
          isChecked: false
        },
        {
          name: 'Google Music',
          imgSrc: 'https://img.icons8.com/color/50/000000/google-play-music.png',
          isChecked: false
        }
      ],
    };
    this.onSearch = this.onSearch.bind(this);
    this.onClick = this.onClick.bind(this);
    this.onCheck = this.onCheck.bind(this);
  };

  onSearch(value) {
    this.setState({
      search: value
    })
  };

  onClick() {
    getItunes(this.state.search.replace(' ', '+'), (result) => this.setState({ listItunes: result.results }));
    getSpotify(this.state.search.replace(' ', '%20'), (result) => this.setState({ listSpot: result.artists.items }));
    /*getDeezer(this.state.search, (result) => this.setState({list: result}));*/
  };

  onCheck(serviceName) {
    const { services } = this.state;
    let newServices = services.map(service => {
      if (service.name === serviceName) {
        service.isChecked = !service.isChecked;
      }

      return service;
    });
    this.setState({ services: newServices })
  }

  render() {
    const { listSpot, listItunes, services } = this.state;
    return (
      <div>
        <SearchBox onSearch={this.onSearch} onClick={this.onClick} services={services} onCheck={this.onCheck} />
        <Div>
          <CardColumn>
            {listSpot.map(({ images, genres, name, external_urls }) =>
              <Card
                artworkUrl100={(images[0] || {}).url} primaryGenreName={genres[0]}
                artistName={name} artistViewUrl={external_urls.spotify} />)}
          </CardColumn>
          <CardColumn>
            {listItunes.map(({ images, primaryGenreName, artistName, artistLinkUrl }) =>
              <Card
                artworkUrl100={images || {}} primaryGenreName={primaryGenreName}
                artistName={artistName} artistViewUrl={artistLinkUrl} />)}
          </CardColumn>
        </Div>
      </div>
    )
  };
}