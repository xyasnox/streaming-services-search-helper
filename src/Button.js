import React from 'react';
import styled from 'styled-components';

const SearchButton = styled.button`
  color: #fff;
  font-weight: bolder;
  padding: 0.25em 1em;
  border: 4px solid #1aaf55;
  border-radius: 4px;
  background-color: #1aaf55;
  margin-left: -4px;
  outline-style: none;
  
  &: hover {
    border: 4px solid #1a7d55;
    background-color: #1a7d55;
  }
`;


const Button = ({
                 onClick,
               }) =>
    <SearchButton onClick={() => onClick()}> Ok, find </SearchButton>;

export default Button;