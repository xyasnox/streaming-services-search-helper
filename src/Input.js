import React from 'react';
import styled from 'styled-components';

const SearchInput = styled.input`
  color: #1a7d55;
  padding: 0.25em 1em;
  border: 4px solid #1aaf55;
  border-radius: 4px;
  background-color: white;
  outline: none;
  cursor: text;
  font: 400 13.3333px Arial;
  font-weight: bolder;
  
  &: focus {
    border: 4px solid #1a7d55;
    background-color: #1a7d55;
    color: #fff;
  }
`;

const Input = ({
                 onSearch,
               }) =>
  <SearchInput onChange={(e) => onSearch(e.target.value)} type='text' />;

export default Input;