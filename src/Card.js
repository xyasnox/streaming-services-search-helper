import React from 'react';
import styled from 'styled-components';

const CardBody = styled.div`
  background-color: white;
  display: inline-block;
  width: 260px;
  border: 4px solid #1a7d55;
  margin-right: 10px;
  margin-top: 10px;
  text-align: center;
  position: relative;
  float: left;
  justify-content: center;
\`;
`;


const Img = styled.img`
  width: 250px;
  height: 250px;
  padding: 0;
  margin: 0;
`;

const Genre = styled.p`
  font-size: 15px;
  background-color: #1a7d55;
  color: #fff;
  padding: 0;
  margin: 0;

  &: hover {
    border: 4px solid #1a7d55;
    background-color: #1a7d55;
  }
`;

const Name = styled.a`
  background-color: #1a7d55;
  color: #fff;
  padding: 0;
  margin: 0;
  display: block;
`;

const Card = ({
                artworkUrl100,
                primaryGenreName,
                artistName,
                artistViewUrl,
              }) =>
  <CardBody>
    <Name href={artistViewUrl}>{artistName}</Name>
    <a href={artistViewUrl}><Img src={artworkUrl100} /></a>
    <Genre>{primaryGenreName}</Genre>
  </CardBody>;

export default Card;